<!-- 
Set the title: <your topic>
Example: Prometheus SLO 
-->

# Task

<!--  Please describe your task in detail. @-mention users for specific asks. -->

## ☕ Area

<!-- Pick all which apply. -->

- [ ] Keptn
- [ ] Prometheus
- [ ] GitLab
- [ ] Kubernetes

## 💡 Description

<!-- What is this issue about? Include all relevant details and URLs to work async. -->

## ✍ Relevant URLs

- [Website]()
- [Docs]()

## 💻 Resources

<!-- What is needed to achieve the goal outlined in this issue. -->

- [ ] Code/Implementation
- [ ] Pair programming/debugging
- [ ] Marketing (Blog, Social)
- [ ] Others: Please specify

<!-- Actions - DO NOT EDIT -->
/label ~"group::keptn" 

/cc @everyonecancontribute/keptn
