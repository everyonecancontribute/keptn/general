# Keptn, GitLab and Prometheus Collaboration Group

This is the organisation tracker for TODOs.

## Resources

- [Issue Board](https://gitlab.com/everyonecancontribute/keptn/general/-/boards/2426230)
- [Agenda](https://docs.google.com/document/d/1TPZl-EMg9qRIO4LE197hAaP99DEVZQoJ1VFb2yf6dBU/edit)

## Workflow

- Create new issues using the [new-task](https://gitlab.com/everyonecancontribute/keptn/general/-/issues/new?issuable_template=new-task) template.
- Use the `group::keptn` label for new issues. This is applied automatically in the `new-task` template.


